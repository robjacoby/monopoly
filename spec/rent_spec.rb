require 'models/property'
require 'models/player'
require 'models/board/square'
require 'rspec/expectations'
require 'models/rent'

describe Rent do
  before(:each) do
    #name, cost, colour_group, mortgage_value, rent, board_position
    Property.new('Whitechapel Road', 60, 'brown', 30, 4, 3)
    Property.list[0].bought_by(Player.list[1])
  end

  describe '.pay' do
    it 'Transfers the correct amount of funds in accordance with the rent value to the appropriate players' do
      # Properties
      property = Property.list[0]
      # Players
      nate = Player.new('Nate', 'Iron', 1500, 1, false)
      john = Player.new('John', 'Dog', 1500, 1, false)
      puts property.rent
      puts nate.funds
      Rent.pay(nate, john, property.rent)
      expect(nate.funds).to eq(1500 - property.rent)
      expect(john.funds).to eq(1500 + property.rent)
    end
  end
end