class Buy

  def self.call(player, property)
    player.buy_property(property)
    property.bought_by(player)
  end

end