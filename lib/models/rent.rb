class Rent

  def self.pay(payer, payee, amount)
    payer.pay_player(payee, amount)
  end
end