class Player
  @@players = Array.new # Creates a Class Instance Variable
  attr_accessor :name, :token, :funds, :position_on_board, :in_jail
  attr_reader :owned_properties, :current_property, :location

  def self.list # Class Method to list all Elements in the Class Instance Variable Array
    @@players
  end

  def initialize(name, token, funds, position_on_board, in_jail)
    # Setting Instance Variable Values to Local Variables
    @name = name
    @token = token
    @funds = funds
    @position_on_board = position_on_board
    @in_jail = in_jail
    @owned_properties = []
    @completed_set = {:set => '', :property => ''}
    @@players << self # Saves the value to the Class Instance Variable
  end

  def display_info
    square = Square.list[@position_on_board.to_i - 1]
    puts "Current Player Details:\n\n#{@name} | Funds avail. $#{@funds} | Current Position: #{square.name}\n\nOwned Properties:\n\n"
    @owned_properties.each { |n| puts "#{n.name}\n" } if @owned_properties.any?
  end

  def buy_property(property)
    @owned_properties << property
    @funds -= property.cost
  end

  def pay_player(player_owed, amount)
    @funds -= amount
    player_owed.funds += amount
  end

  def pass_go
    @funds += 200
  end

  def move(spaces)
    board_total = Square.list.length
    if @position_on_board + spaces > board_total
      @position_on_board += spaces - board_total
    else
      @position_on_board += spaces
    end
  end

  def has_moved
    @location = Square.list[@position_on_board.to_i - 1]
  end

  def current_property(location)
    @current_property = Property.list[location.position]
  end

  def owned_sets(group, property)
    @completed_sets = {group => property}
    puts @completed_sets
  end

  def in_jail?
    @in_jail
  end

  def send_to_jail
    puts "You've been sent to jail #{@name}! Your turn has now ended"
    @position_on_board = 10
  end
end