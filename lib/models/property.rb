class Property
  attr_accessor :name, :cost, :colour_group, :mortgage_value, :rent, :board_position, :owned_by_player
  @@properties = Array.new

  def initialize(name, cost, colour_group, mortgage_value, rent, board_position)
    @name = name
    @cost = cost
    @colour_group = colour_group
    @mortgage_value = mortgage_value
    @rent = rent
    @for_sale = true
    @owned_by_player = nil
    @board_position = board_position
    @@properties << self
    #
    Square.new(board_position, name, self)
    # ## Creates the association with the Square
    # I originally had Square.new(board_position, name, true)
    #
    # the boolean attribute was to determine whether the square was a property or not...
    #
    # but then found out I could pass 'self'
    # mind = blown!
  end

  def completed_set?(unique)
    unique.any? && unique.length == 1
  end

  def self.list
    @@properties
  end

  def bought_by(player)
    @owned_by_player = player
    @for_sale = false
  end

  def purchase(player)
    if player.funds >= @cost
      Buy.call(player, self)
      puts "\n#{@name} now belongs to #{player.name}\n"
    else
      puts 'You lack the necessary funds :('
    end
  end

  def is_owed_rent?(player, property)
    player != property.owned_by_player
  end

  def for_sale?
    @for_sale
  end

  def is_property?
    true
  end
end