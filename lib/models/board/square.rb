class Square
  attr_accessor :position, :name, :type

  @@squares = Array.new

  def initialize(position, name, type)
    @position = position
    @name = name
    @type = type
    @@squares << self
  end

  def self.list
    @@squares.sort_by { |square| square.position }
  end

end
