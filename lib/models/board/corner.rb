class Corner

  class PassGo

    def initialize
      #(position, name, property?)
      Square.new(1, 'Go', false)
    end

    def is_property?
      false
    end

  end

  class Jail

    def initialize
      #(position, name, property?)
      Square.new(10, 'Jail', false)
    end

    def is_property?
      false
    end
  end
end