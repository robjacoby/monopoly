class Turn
  attr_accessor :current_player_index

  def initialize(current_player_index)
    @ended = false
    @current_player_index = current_player_index
    @current_player = Player.list[current_player_index.to_i]
  end

  def start
    player = @current_player
    player.display_info
    request_roll
    roll_dice
    puts "Player was on position #{player.position_on_board}"
    player.move(@roll_result)
    puts puts "Player is now on position #{player.position_on_board}"
    player.has_moved
    if player.location.type.is_property?
      property_decision(player.location.type)
    else
      puts 'Not a property'
      end_turn
    end
    end_turn
  end

  def property_decision(property)
    player = @current_player
    if property.for_sale?
      purchase_request(property)
    else
      puts "Uh-oh! #{property.name} is already owned by #{property.owned_by_player.name}\nThey collect $#{property.rent} rent from you\n\n"
      Rent.pay(player, property.owned_by_player, property.rent) if property.is_owed_rent?(player, property)
    end
  end

  def end_turn
    player = @current_player
    puts "\nYou have $#{player.funds} remaining\n\n"
    @ended = true
  end

  def ended?
    @ended
  end

  def play
    start
  end

  def request_roll
    rsp = ''
    until rsp == 'roll'
      puts "type 'roll' into the console to begin your turn"
      rsp = gets.chomp
    end
  end

  def purchase_request(property)
    rsp = ''
    until rsp == 'y' || rsp == 'n'
      puts "Would you like to purchase #{property.name} for $#{property.cost}? y/n"
      rsp = gets.chomp
    end
    if rsp == 'y'
      property.purchase(@current_player)
    end
  end

  def roll_dice
    roll = Roll.new
    @roll_result = roll.result
    puts "\nYou rolled for: #{roll.result.to_s}"
  end
end