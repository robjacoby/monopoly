require_relative '../bin/require'

class Game
  attr_accessor :current_player_index

  def initialize
    Seed.players
    Seed.properties
    Board.new
    @current_player_index = 0
  end

  def change_current_player
    if @current_player_index == (Player.list.length - 1)
      @current_player_index = 0
    else
      @current_player_index += 1
    end
  end

  def run
    puts "The game has begun!\n\n"
    until ended? do
      turn = Turn.new(@current_player_index)
      process(turn)
      change_current_player
    end
  end

  def process(turn)
    until turn.ended? do
      turn.play
    end
  end

  def ended?
    Player.list.length == 0
  end
end