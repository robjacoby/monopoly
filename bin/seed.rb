class Seed

  def self.players
    # (name, token, funds, position_on_board, in_jail?)
    Player.new('Nate', 'Iron', 1500, 1, false)
    Player.new('John', 'Dog', 1500, 1, false)
    Player.new('Scott', 'Car', 1500, 1, false)
    Player.new('Joe', 'Battleship', 1500, 1, false)
  end

  def self.properties
    #( name, Cost, Colour, Mortgage Value, Rent, Complete?, Mortgaged? For Sale?, Board_position)
    Property.new('Old Kent Road', 60, 'brown', 30, 2, 2)
    Property.new('Whitechapel Road', 60, 'brown', 30, 4, 3)
    Property.new('The Angel Islington', 100, 'light_blue', 50, 6, 4)
    Property.new('Euston Road', 100, 'light_blue', 50, 6, 5)
    Property.new('Pentonville Road', 120, 'light_blue', 60, 8, 6)
    Property.new('Pall Mall', 140, 'pink', 70, 10, 7)
    Property.new('Whitehall', 140, 'pink', 70, 10, 8)
    Property.new('Northumberland Avenue', 160, 'pink', 80, 12, 9)
  end

end