# Monopoly™ (UK Edition)

An implementation of the Monopoly™ board game by the Parker Brothers written in Ruby

## Code Exercise Requirements


Implement a game of monopoly using Ruby. You will need to decide on how to represent players, the board, properties, as well as the rules and other elements of the game.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

This project assumes you are using the following version of ruby:

```
ruby 2.4.0
```

NB: This isn't to say it won't work with a different version, but no promises can be made. 

### Installing

Clone the project file

```
git clone https://gitlab.com/djcuze/monopoly.git
```

Navigate to the project's root directory
```
cd PROJECT-DIR
```

Install the required gems

```
bundle install
```

To run the program from your console
```
Game.new.run
```
NB: This will automatically populate the board, create four (4) individual players and begin the first turn. 

## Running the tests

Testing for this project is performed by the *RSpec* gem.

To run the test suite simply type ```rspec``` into the terminal

# Future Plans

Here's a to-do list of the things I ran out of time to implement or complete but want to in the future:

- Defining the Jail square, and appropriate "Send to Jail" methods
- Tax Tiles
- Writing the function to determine whether all properties of a certain group are owned by a single player, and enabling them to not only collect double rent (as per the rulebook) but also purchase property improvements.
- Community Chest & Chance Cards 
- Implementing Mortgaging and all the methods and functions it would require
- Properly defining and programming bankruptcy. ie. Giving the option if the player is able to mortgage properties when their funds reach the 0 mark and they are required to pay another player/the bank. 
- Implementing the change of rent according to the number of houses/hotels on properties.

- **When it is completed, I would love to attach a front-end.**
  
#### I plan to continue working on this application as a personal project and to continue improving my skills and knowledge of basic Ruby as I extremely enjoyed building this application. 

# Things I Learned in the Process

Before I began this project, my knowledge of Ruby was quite limited. *"Enough to get by"* if you will. 

At the end though, I learned: 

- The differences between the many types of variables, and how they performed. 
- That using class Methods for everything was baaaaaaaaad. 
- To put a good amount of thought and time into the naming of methods
- To start thinking about which method belonged to which class:
- How to write loops
- That all methods have an output, but deciding where it was appropriate for them to receive an input
- How to compartmentalize large amounts of code
- How to use the select and map methods for Arrays and Hashes
- I learned what RSpec was and how to write a basic test in it
- How to structure a directory without using Rails as a crutch
- Entities and how they relate to each other, and how to plan as such

# Project Timeline
In order to keep the project at a small enough scale, I began with 4 players, 10 squares (Go, Jail and 8 properties), and I used only one dice that could roll a maximum of 6.

I plan to expand to the full board range as I continue working on the application.

1. I started the project with a notepad and a pen; planning out which entities I required and what information I needed them to contain.
   - *I created a table of the board, writing what each square was, the cost of the tiles etc.* 
2. I then created a new directory and began creating the empty classes I believed I needed
3. Next I created the Player and Property entities and their initalize functions
4. Defined a Game
5. Defined a Turn
6. Defined a Square
7. Defined the Dice and the roll
8. I created a Seed class, where I created the 8 properties and 4 players
9. I discovered my requirements where getting too long so I moved them to their own file and just required that file in the Game class
10. I then began mixing it all together. I began to define the turn, and how it would play out, although at this time I was using class methods.  
ie: Dice.roll, Turn.start, Turn.end.  ()Everything became quite messy) 
11. I was feeling frustrated with how the project was evolving and my mentor suggested I learn about the difference between instance and class methods.  
12. With this new knowledge, I went back and rewrote most of my code to begin passing info back and forth and making it a lot cleaner and work more efficiently. 
13. Once I had a full 'Turn' complete, I created a method to change to the next player (as before I had been assigning that to a global variable). 
14. I was unsure how to determine whether a turn was complete or not. My mentor suggested I research loops as a way of solving this problem. Armed with this new information, I created a method and loop for the Game to determine when a Turn was ended or not, and when it was, to run another method to change the current player that would then be passed back to the New Turn method.
15. I then created the Buy method for properties, and created empty arrays to store the required Player instance within the `@owned_by` array within Property. Similarly, the Property instance was added to the `@owned_properties` array for the Player.
16. After this, I created the method to determine whether or not the property that was landed on was already owned by another player, and created the Rent methods, so that funds would transfer between players appropriately.
17. At this stage, I realised a fatal flaw in my code in regards to deciding where a Square was a property and how to act accordingly. 
    - before this I was individually assigning a board position to each creation of a property instance. I then discovered that I was able to pass `self` into the Square.new method which I called in the `initialize` method of the `Property` class
18. I began writing a function to determine if all properties of a given set had been purchased by the same player, which I intended to continue with then allowing that player to begin purchasing improvements. I have not yet completed this function as I've run out of time.
    
## Beginning a Game

```Game.new.run``` - Begins a new game. 

All requirement files are stored in ```'/bin/require.rb```

The `Game.new` method also calls upon the `Seed` class, which contains methods that create the instances of `Players` and `Properties` in order to properly pre-populate the board.  

## Authors

* **Nate Jacoby** - *Initial work* - [Gitlab Profile](https://gitlab.com/djcuze) 

## License

This project is licensed under the MIT License

## Acknowledgments

A massive thank you to the following:
* **Rob Jacoby**
* **Tom Ridge**

Who both mentored me and assisted with any questions I had along the way
